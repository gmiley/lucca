/*
 * Lucca.c
 *
 * Created: 9/22/2016 4:09:24 PM
 * Author : gmiley
 */ 

#include <avr/io.h>
#include "common/shared.h"
#include "common/menu.h"

const char welcome[] = "Hello, I'm Lucca. Press 'B' to continue!";


// Data for generating the characters used in load_custom_characters
// and display_readings.  By reading levels[] starting at various
// offsets, we can generate all of the 7 extra characters needed for a
// bargraph.  This is also stored in program space.
const char levels[] PROGMEM = {
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b11111,
	0b11111,
	0b11111,
	0b11111,
	0b11111,
	0b11111,
	0b11111
};

// This function loads custom characters into the LCD.  Up to 8
// characters can be loaded; we use them for 7 levels of a bar graph.
void load_custom_characters()
{
	lcd_load_custom_character(levels+0,0); // no offset, e.g. one bar
	lcd_load_custom_character(levels+1,1); // two bars
	lcd_load_custom_character(levels+2,2); // etc...
	lcd_load_custom_character(levels+3,3);
	lcd_load_custom_character(levels+4,4);
	lcd_load_custom_character(levels+5,5);
	lcd_load_custom_character(levels+6,6);
	clear(); // the LCD must be cleared for the characters to take effect
}

void init()
{
	
	pololu_3pi_init(2000);

	
	// Notify init complete.
	//play_from_program_space(PSTR(">g32>>c32"));
	play_from_program_space(PSTR("gaf<f-c."));
	//delay_ms(5000);
	//play_from_program_space(PSTR("b-ca-<a-"));
	scroll_print((char*)welcome, sizeof(welcome)/sizeof(welcome[0]),(void*){"\0"}, 1, 1);
}

int main(void)
{
	init();

	MenuSelection menu_choice = present_menu();
	
	clear();
	print("L1: ");
	print_long(menu_choice.L1);
	lcd_goto_xy(0, 1);
	print("L2: ");
	print_long(menu_choice.L2);
	char button = wait_for_button_press(ANY_BUTTON);
	wait_for_button_release(button);
	
    while (1) 
    {
		int tks = get_ticks();
		int prev_time = 0;
		int cur_time = 0;
		while(button_is_pressed(BUTTON_B) != BUTTON_B)
		{
			prev_time = cur_time;
			cur_time = time_from_start(tks, SECONDS);
			if(prev_time != cur_time)
			{
				clear();
				print("T-Sec: ");
				print_long(cur_time);
			}
		}
    }
}

