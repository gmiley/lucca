/*
 * shared.c
 *
 * Created: 9/22/2016 4:15:09 PM
 *  Author: gmiley
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "shared.h"

void calibrate_gir_sensors(unsigned int* sensors)
{
	
}

/*
 *	void scroll_print
 *	(
 *		char* message1,
 *			// First line (row) of text.
 *		int   message1_len,
 *			// Length of the message1 array.
 *		char* message2,
 *			// Seconds line (row) of text.
 *		int   message1_len,
 *			// Length of the message2 array.
 *		int   wrap
 *			// Toggles (1 or 0) if the messages should wrap back to the beginning when the
 *			// end is reached, or reverse the scroll direction.
 *	)
 *
 *	Note:	While the message is scrolling, press B to break out of the display; press A
 *			to jump back to the beginning of the message(s); press C to jump to the end
 *			of the message(s).
 */
void scroll_print(char* message1, int message1_len, char* message2, int message2_len, int wrap)
{

	int maxlen = (message1_len >= message2_len)?message1_len:message2_len;
	int tks = get_ticks(), prev_time = 0, cur_time = 0;
	int offset = 0, increment = 1, lcd_width = 8;
	char button = 0;
	if(maxlen <= lcd_width)
	{
		while((button = button_is_pressed(ANY_BUTTON)) != BUTTON_B)
		{
			clear();
			if(message1_len>0) print(message1);
			if(message1_len>0 && message2_len>0) lcd_goto_xy(0, 1);
			if(message2_len>0) print(message2);
			delay_ms(200);
		}
		wait_for_button_release(button);
	}
	else
	{
		char message1_print[lcd_width],message2_print[lcd_width];
		while((button = button_is_pressed(ANY_BUTTON)) != BUTTON_B)
		{
			switch(button)
			{
				case BUTTON_A:
				offset = 0;
				break;
				case BUTTON_C:
				offset = maxlen-lcd_width;
				break;
			}
			if(button != 0)
			{
				wait_for_button_release(button);
				if(button != BUTTON_B) button = 0;
			}
			prev_time = cur_time;
			cur_time = time_from_start(tks, MILLISECONDS)/300;
			if(prev_time != cur_time)
			{
				clear();
				if(message1_len>0)
				{
					if(offset < message1_len) memcpy(message1_print, message1+offset, lcd_width * sizeof(message1[0]));
					else memset(message1_print, 0, message1_len);
					print(message1_print);
				}
				lcd_goto_xy(0,1);
				if(message1_len>0)
				{
					if(offset < message2_len) memcpy(message2_print, message2+offset, lcd_width * sizeof(message2[0]));
					else memset(message2_print, 0, message2_len);
					print(message2_print);
				}
				if(offset+lcd_width >= maxlen)
					if(wrap) offset = -1;
					else increment = -1;
				else if(offset <= 0) increment = 1;
				offset += increment;
			}
		}
		wait_for_button_release(button);
		delay_ms(200);
	}
}

unsigned long time_from_start(unsigned long startTicks, TimeUnitType returnUnitType)
{
	unsigned long retval = (get_ticks()-startTicks);
	retval = ticks_to_microseconds(retval);
	switch(returnUnitType)
	{
		default:
		case SECONDS:
		retval = retval / 1000000;
		break;
		case MILLISECONDS:
		retval = retval / 1000;
		break;
		case MICROSECONDS:
		retval = retval;
		break;
	}
	return retval;
}


