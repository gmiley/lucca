/*
 * menu.h
 *
 * Created: 9/24/2016 8:35:37 AM
 *  Author: gmiley
 */ 

#include "shared.h"

#ifndef MENU_H_
#define MENU_H_

typedef enum
{
	Menu_Cancel = 0,
	Menu_Calibrate = 1,
	Menu_Test = 2,
} MenuOptions;

typedef enum
{
	Menu_Calibrate_Cancel = 0,
	Menu_Calibrate_Sensors = 1,
	Menu_Calibrate_Turning = 2,
	Menu_Calibrate_Movement = 3,
} SubMenuCalibrateOptions;

typedef enum
{
	Menu_Test_Cancel = 0,
	Menu_Test_Sensors = 1,
	Menu_Test_Turning = 2,
	Menu_Test_Movement = 3,
} SubMenuTestOptions;

typedef struct MenuSelection
{
	int L1;
	int L2;
} MenuSelection;


MenuSelection present_menu();
int menu_l2_selector(MenuOptions menu_l1_selection);
int menu_selector(char menu[][8], int menu_size);
SubMenuCalibrateOptions menu_l2_calibrate();
SubMenuTestOptions menu_l2_test();
MenuOptions menu_l1();



#endif /* MENU_H_ */