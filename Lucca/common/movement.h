/*
 * movement.h
 *
 * Created: 9/22/2016 4:26:50 PM
 *  Author: gmiley
 */ 


#ifndef MOVEMENT_H_
#define MOVEMENT_H_
const int speed[3] = { 0, 100, 255 };

typedef enum 
{
	Stop = 0,
	Normal = 1,
	Max = 2,
} MoveSpeed;


#endif /* MOVEMENT_H_ */