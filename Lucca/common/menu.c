/*
 * menu.c
 *
 * Created: 9/24/2016 8:35:26 AM
 *  Author: gmiley
 */ 

#include "menu.h"

MenuSelection present_menu()
{
	MenuSelection retval = {0, 0};
	do 
	{
		retval.L1 = (int)menu_l1();
		retval.L2 = (int)menu_l2_selector(retval.L1);
	} while (retval.L1 == 0 || retval.L2 == 0);
	return retval;
}

int menu_l2_selector(MenuOptions menu_l1_selection)
{
	int retval = 0;
	switch(menu_l1_selection)
	{
		case Menu_Calibrate:
		retval = menu_l2_calibrate();
		break;
		case Menu_Test:
		retval = menu_l2_test();
		break;
		case Menu_Cancel:
		retval = 0;
		break;
	}
	return retval;
}

int menu_selector(char menu[][8], int menu_size)
{
	unsigned char button = 0;
	int menu_item = 1;
	do
	{
		clear();
		print(menu[menu_item]);
		lcd_goto_xy(0,1);
		print("<  B  >");
		button = wait_for_button_press(ANY_BUTTON);
		wait_for_button_release(button);
		if(button == BUTTON_A)
		{
			menu_item--;
			if(menu_item < 0) menu_item = menu_size-1;
		}
		else if(button == BUTTON_C)
		{
			menu_item++;
			if(menu_item >= menu_size) menu_item = 0;
		}
	} while(button != BUTTON_B);
	return menu_item;
}

SubMenuCalibrateOptions menu_l2_calibrate()
{
	char menu[4][8] = { {"Cancel"}, {"Sensors"}, {"Turning"}, {"Movement"} };
	return (SubMenuCalibrateOptions)menu_selector(menu, sizeof(menu)/sizeof(menu[0]));
}

SubMenuTestOptions menu_l2_test()
{
	char menu[5][8] = { {"Cancel"}, {"Sensors"}, {"Turning"}, {"Movement"}, {"Go Line"} };
	return (SubMenuTestOptions)menu_selector(menu, sizeof(menu)/sizeof(menu[0]));
}

MenuOptions menu_l1()
{
	char menu[3][8] = { {"Cancel"}, {"Calibrtn"}, {"Test"} };
	return (MenuOptions)menu_selector(menu, sizeof(menu)/sizeof(menu[0]));
}
