/*
 * shared.h
 *
 * Created: 9/22/2016 4:14:59 PM
 *  Author: gmiley
 */ 
#include <avr/io.h>
#include <pololu/3pi.h>

#ifndef SHARED_H_
#define SHARED_H_

extern unsigned int gir_sensor[5];
extern unsigned int gir_sensor_threshold[5]; // = {300, 300, 300, 300, 300};

typedef enum
{
	INCHES = 0,
	CENTIMETERS = 1,
} LengthUnitType;

typedef enum
{
	SECONDS = 0,
	MILLISECONDS = 1,
	MICROSECONDS = 2,
} TimeUnitType;

void calibrate_gir_sensors(unsigned int*);
void scroll_print(char*, int, char*, int, int);
unsigned long time_from_start(unsigned long, TimeUnitType);

#endif /* SHARED_H_ */